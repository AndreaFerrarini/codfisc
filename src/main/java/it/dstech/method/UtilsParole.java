package it.dstech.method;

public class UtilsParole {

	public static int numeroConsonanti(String cognome) {
		
		int consonanti = 0;
		for (int i = 0; i < cognome.length(); i++) {
			if (!vocale(cognome.charAt(i))) {
				consonanti++;
			}
		}
		return consonanti;
	}

	public static String primeConsonanti(String cognome, int numero) {
		
		String consonanti = "";
		for (int i = 0; i < cognome.length(); i++) {
			if (!vocale(cognome.charAt(i))) {
				if (consonanti.length() < numero) {
					consonanti += Character.toString(cognome.charAt(i));
				}
			}
		}
		return consonanti;
	}

	public static String conteggio(String string, int i) {
		int cont = 0;
		for (int j = 0; j < string.length(); j++) {
			if (!vocale(string.charAt(j))) {
				cont++;
				if (cont == i) {
					return Character.toString(string.charAt(j));
				}
			}
		}
		return null;
	}

	public static String primeVocali(String string, int numero) {
		String vocali = "";
		for (int i = 0; i < string.length(); i++) {
			if (vocale(string.charAt(i))) {
				if (vocali.length() < numero) {
					vocali += Character.toString(string.charAt(i));
				}
			}
		}
		return vocali;
	}

	public static String addX(int n) {
		
		String risultato = "";
		for (int i = 0; i < n; i++) {
			risultato += "X";
		}
		return risultato;
	}

	public static String eliminaSpaziBianchi(String string) {
		return string.replaceAll("\\s+", "");
	}

	public static boolean vocale(char character) {

		String vocali = "AEIOU";

		return vocali.contains(Character.toString(character));
	}

	public static String stringaPari(String string) {
		String risultato = "";
		for (int i = 0; i < string.length(); i++) {
			if ((i + 1) % 2 == 0) {
				risultato += Character.toString(string.charAt(i));
			}
		}
		return risultato;
	}

	public static String stringaDispari(String string) {
		String risultato = "";
		for (int i = 0; i < string.length(); i++) {
			if ((i + 1) % 2 == 1) {
				risultato += Character.toString(string.charAt(i));
			}
		}
		return risultato;
	}

}
