package it.dstech.services;

import it.dstech.models.Persona;

public interface PersonaService {

	public Persona search(Long id);

	public boolean create(Persona persona);

	public boolean delete(Persona persona);

	public boolean update(Persona persona);
}
