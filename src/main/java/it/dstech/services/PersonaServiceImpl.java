package it.dstech.services;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.dstech.dao.PersonaDao;
import it.dstech.method.UtilsParole;
import it.dstech.models.Persona;

@Service
@Transactional
public class PersonaServiceImpl implements PersonaService {

	@Autowired
	PersonaDao dao;

	@Override
	public Persona search(Long id) {

		return dao.search(id);
	}

	@Override
	public boolean create(Persona persona) {

		calcolaCF();

		return dao.create(persona);

	}

	@Override
	public boolean delete(Persona persona) {

		return dao.delete(persona);
	}

	@Override
	public boolean update(Persona persona) {

		return dao.update(persona);
	}

	public String calcoloCognomeCf(String cognome) {

		String codiceCognome;
		int numeroConsonanti;
		cognome = UtilsParole.eliminaSpaziBianchi(cognome).toUpperCase();

		if (cognome.length() >= 3) {

			numeroConsonanti = UtilsParole.numeroConsonanti(cognome);

			if (numeroConsonanti >= 3) {

				codiceCognome = UtilsParole.primeConsonanti(cognome, 3);
			} else {

				codiceCognome = UtilsParole.primeConsonanti(cognome, numeroConsonanti);
				codiceCognome += UtilsParole.primeVocali(cognome, 3 - numeroConsonanti);
			}
		} else {

			int numeroCaratteri = cognome.length();
			codiceCognome = cognome + UtilsParole.addX(3 - numeroCaratteri);
		}

		return codiceCognome;
	}

	public String calcolaCodiceNome(String nome) {

		String codiceNome;
		int numeroConsonanti;
		nome = UtilsParole.eliminaSpaziBianchi(nome).toUpperCase();

		if (nome.length() >= 3) {

			numeroConsonanti = UtilsParole.numeroConsonanti(nome);

			if (numeroConsonanti >= 4) {

				codiceNome = UtilsParole.conteggio(nome, 1) + UtilsParole.conteggio(nome, 3)
						+ UtilsParole.conteggio(nome, 4);
			} else if (numeroConsonanti >= 3) {

				codiceNome = UtilsParole.primeConsonanti(nome, 3);
			} else {

				codiceNome = UtilsParole.primeConsonanti(nome, numeroConsonanti);
				codiceNome += UtilsParole.primeVocali(nome, 3 - numeroConsonanti);
			}
		} else {

			int numeroCaratteri = nome.length();
			codiceNome = nome + UtilsParole.addX(3 - numeroCaratteri);
		}

		return codiceNome;
	}

	public String calcolaCodiceDataNascitaESesso(int anno, int mese, int giorno, String sesso) {

		String codDataNascitaESesso;
		String codAnno;
		String codMese;
		String codGiornoESesso;

		codAnno = calcolaCodiceAnno(anno);
		codMese = calcolaCodiceMese(mese);
		codGiornoESesso = calcolaCodiceGiornoESesso(giorno, sesso);

		codDataNascitaESesso = codAnno + codMese + codGiornoESesso;

		return codDataNascitaESesso;
	}

	public String calcolaCodiceAnno(int anno) {

		return Integer.toString(anno).substring(2);
	}

	private String calcolaCodiceMese(int mese) {

		String risultato;
		mese++;

		switch (mese) {
		case 1:
			risultato = "A";
			break;
		case 2:
			risultato = "B";
			break;
		case 3:
			risultato = "C";
			break;
		case 4:
			risultato = "D";
			break;
		case 5:
			risultato = "E";
			break;
		case 6:
			risultato = "H";
			break;
		case 7:
			risultato = "L";
			break;
		case 8:
			risultato = "M";
			break;
		case 9:
			risultato = "P";
			break;
		case 10:
			risultato = "R";
			break;
		case 11:
			risultato = "S";
			break;
		case 12:
			risultato = "T";
			break;
		default:
			risultato = "";
			break;
		}
		return risultato;
	}

	public String calcolaCodiceGiornoESesso(int giorno, String sesso) {

		String codiceGiorno = String.format("%02d", giorno);

		if (sesso.equals("F")) {
			int codiceGiornoIntero;
			codiceGiornoIntero = Integer.parseInt(codiceGiorno);
			codiceGiornoIntero += 40;
			codiceGiorno = Integer.toString(codiceGiornoIntero);
		}

		return codiceGiorno;
	}

	public String calcolaCarattereDiControllo(String codice) {

		String pari = UtilsParole.stringaPari(codice);
		String dispari = UtilsParole.stringaDispari(codice);

		int sommaDispari = conversioneCaratteriDispari(dispari);
		int sommaPari = conversioneCaratteriPari(pari);

		int somma = sommaDispari + sommaPari;
		int resto = (int) somma % 26;
		char restoConvertito = conversioneResto(resto);

		return Character.toString(restoConvertito);
	}

	public int conversioneCaratteriDispari(String string) {
		int risultato = 0;
		for (int i = 0; i < string.length(); i++) {
			char carattere = string.charAt(i);
			switch (carattere) {
			case '0':
			case 'A':
				risultato += 1;
				break;
			case '1':
			case 'B':
				risultato += 0;
				break;
			case '2':
			case 'C':
				risultato += 5;
				break;
			case '3':
			case 'D':
				risultato += 7;
				break;
			case '4':
			case 'E':
				risultato += 9;
				break;
			case '5':
			case 'F':
				risultato += 13;
				break;
			case '6':
			case 'G':
				risultato += 15;
				break;
			case '7':
			case 'H':
				risultato += 17;
				break;
			case '8':
			case 'I':
				risultato += 19;
				break;
			case '9':
			case 'J':
				risultato += 21;
				break;
			case 'K':
				risultato += 2;
				break;
			case 'L':
				risultato += 4;
				break;
			case 'M':
				risultato += 18;
				break;
			case 'N':
				risultato += 20;
				break;
			case 'O':
				risultato += 11;
				break;
			case 'P':
				risultato += 3;
				break;
			case 'Q':
				risultato += 6;
				break;
			case 'R':
				risultato += 8;
				break;
			case 'S':
				risultato += 12;
				break;
			case 'T':
				risultato += 14;
				break;
			case 'U':
				risultato += 16;
				break;
			case 'V':
				risultato += 10;
				break;
			case 'W':
				risultato += 22;
				break;
			case 'X':
				risultato += 25;
				break;
			case 'Y':
				risultato += 24;
				break;
			case 'Z':
				risultato += 23;
				break;
			}
		}
		return risultato;
	}

	public int conversioneCaratteriPari(String string) {

		int risultato = 0;
		for (int i = 0; i < string.length(); i++) {
			char carattere = string.charAt(i);
			int numero = Character.getNumericValue(carattere);

			if (Character.isLetter(carattere)) {

				numero = carattere - 65;
				risultato += numero;
			} else {

				risultato += numero;
			}

		}
		return risultato;
	}

	public char conversioneResto(int resto) {
		return (char) (resto + 65);
	}

	// vuoto per ora
	public String calcolaCodiceComune(String comune) {

		return comune;
	}

	public String calcolaCF() {

		Persona persona = new Persona();

		String codiceCognome = calcoloCognomeCf(persona.getCognome());
		String codiceNome = calcolaCodiceNome(persona.getNome());
		String codiceDataNascitaESesso = calcolaCodiceDataNascitaESesso(persona.getAnno(), persona.getMese(),
				persona.getGiorno(), persona.getSesso());
		String codiceComunale = calcolaCodiceComune(persona.getComuneDiNascita());

		String risultato = codiceCognome + codiceNome + codiceDataNascitaESesso + codiceComunale;

		String carattereDiControllo = calcolaCarattereDiControllo(risultato);

		risultato += carattereDiControllo;

		return risultato;
	}
}
