package it.dstech.dao;


import it.dstech.models.Persona;

public interface PersonaDao {


	public Persona search(Long id);

	public boolean create(Persona persona);

	public boolean delete(Persona persona);

	public boolean update(Persona persona);
}
